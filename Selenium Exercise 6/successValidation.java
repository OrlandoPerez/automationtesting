package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class successValidation {
	
	@SuppressWarnings("deprecation")
	public void waitAlertBanner(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".alert-success")));
	}

	public String AlertBanner(WebDriver driver) {
		return driver.findElement(By.cssSelector(".alert-success")).getText();
	}

}
