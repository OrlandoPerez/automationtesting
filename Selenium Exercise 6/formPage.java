package Pages;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class formPage {
	
	public void formFill(WebDriver driver, String username, String lastname, String job) throws IOException {
		driver.findElement(By.id("first-name")).sendKeys(username);
		driver.findElement(By.xpath("//input[@id='last-name']")).sendKeys(lastname);
		driver.findElement(By.id("job-title")).sendKeys(job);
		driver.findElement(By.cssSelector("#radio-button-1")).click();
		File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshotFile, new File("C:\\Users\\Perez\\eclipse-workspace\\dataDriven\\src\\Documents\\Radio.png"));
		
		driver.findElement(By.cssSelector("#checkbox-3")).click();
		File screenshotFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshotFile2, new File("C:\\Users\\Perez\\eclipse-workspace\\dataDriven\\src\\Documents\\checkbox.png"));
		
		driver.findElement(By.id("select-menu")).click();
		driver.findElement(By.xpath("//option[contains(text(),'2-4')]")).click();
		File screenshotFile3 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenshotFile3, new File("C:\\Users\\Perez\\eclipse-workspace\\dataDriven\\src\\Documents\\Select.png"));
		
		driver.findElement(By.id("datepicker")).sendKeys("10/09/2021");
		
		driver.findElement(By.xpath("//a[contains(text(),'Submit')]")).click();
		
		
	}

}
