package dataDriven;



import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import Pages.formPage;
import Pages.readExcelFile;
import Pages.successValidation;

public class Main {
	
	
ChromeDriver driver;
int rows;

@Test
	public void testFillForm(){
		System.setProperty("webdriver.chrome.driver", "C://Users/Perez/OneDrive/Documentos/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
	
		readExcelFile  config = new readExcelFile("C:\\Users\\Perez\\eclipse-workspace\\dataDriven\\src\\Documents\\Data.xlsx");
		formPage fp = new formPage();
		try {
			fp.formFill(driver, (String)config.getData(0, 1, 1), (String)config.getData(0, 1, 2), (String)config.getData(0, 1, 3));
			File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshotFile, new File("C:\\Users\\Perez\\eclipse-workspace\\dataDriven\\src\\Documents\\SubmitingS.png"));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		successValidation sv = new successValidation();
		sv.waitAlertBanner(driver);
		assertEquals("The form was successfully submitted!", sv.AlertBanner(driver));
		
		
	}
	

}
