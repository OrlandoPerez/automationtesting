
import static org.testng.Assert.assertEquals;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;




public class Main {
	
	ChromeDriver driver;
	
	
	public void testFillForm() throws InterruptedException{
		System.setProperty("webdriver.chrome.driver", "C://Users/Perez/OneDrive/Documentos/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
	
		formPage fp = new formPage();
		fp.formFill(driver);
	
		successValidation sv = new successValidation();
		sv.waitAlertBanner(driver);
		assertEquals("The form was successfully submitted!", sv.AlertBanner(driver));
		
		
	}

	
	
}
